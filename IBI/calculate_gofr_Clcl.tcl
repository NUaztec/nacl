foreach next_arg $argv {
    set output_filename $next_arg
}

set sel1 [atomselect top "name D"]
set sel2 [atomselect top "name D"]
set delta 0.002
set rmax 1.5
set first 1
set last -1

if {[catch {measure gofr $sel1 $sel2 delta $delta rmax $rmax usepbc 1 first $first last $last} \
         errmsg ] } then {
    $sel1 delete
    $sel2 delete
    return
} else {
    lassign $errmsg rlist glist ilist hlist frlist
    after 3000 {catch {destroy .succmsg}}
}

set outfile [open $output_filename w] 
foreach r $rlist g $glist i $ilist {
    puts $outfile "$r $g $i"
}
close $outfile
exit
