#! /usr/bin/env python
# import argparse
from subprocess import *
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import sys
import numpy as np
print "reading files"
print sys.argv

# import data
AAgofr_Nana = np.genfromtxt('AANana.dat', delimiter=' ')
AAgofr_Nacl = np.genfromtxt('AANacl.dat', delimiter=' ')
AAgofr_Clcl = np.genfromtxt('AAClcl.dat', delimiter=' ')

CGgofr_Nana = np.genfromtxt('gofr_Nana.dat', delimiter=' ')
CGgofr_Nacl = np.genfromtxt('gofr_Nacl.dat', delimiter=' ')
CGgofr_Clcl = np.genfromtxt('gofr_Clcl.dat', delimiter=' ')
'''
oldPMF_Nana = np.genfromtxt('newNana_0.dat', delimiter=' ')
oldPMF_Nacl = np.genfromtxt('newNacl_0.dat', delimiter=' ')
oldPMF_Clcl = np.genfromtxt('newClcl_0.dat', delimiter=' ')
'''

oldPMF_Nana = np.genfromtxt(sys.argv[1], delimiter=' ')
oldPMF_Nacl = np.genfromtxt(sys.argv[2], delimiter=' ')
oldPMF_Clcl = np.genfromtxt(sys.argv[3], delimiter=' ')


dist1 = oldPMF_Nana[:, 0]  # or AA[:,0] first 100 points are already removed
dist2 = oldPMF_Nacl[:, 0]
dist3 = oldPMF_Clcl[:, 0]
length = len(dist1)
print length
F_Nana = np.zeros(length)
F_Nacl = np.zeros(length)
F_Clcl = np.zeros(length)

gAA_Nana = AAgofr_Nana[:, 1]  # the smoothed g of r
gAA_Nacl = AAgofr_Nacl[:, 1]
gAA_Clcl = AAgofr_Clcl[:, 1]

gCG_Nacl = CGgofr_Nacl[100:750, 1]
gCG_Nana = CGgofr_Nana[100:750, 1]
gCG_Clcl = CGgofr_Clcl[100:750, 1]

alpha = 0.8   # iterative correction coefficient
expfactor = 1.1
x = np.linspace(0,1.5,length)
weight = np.exp(-0.5*x*x)
# smooth g of r ----------------------------------------------------------
for j in range(1):  # (order of smoothing)
    print "smooth g"
    temp = gCG_Nana
    for i in range(length - 2):
        gCG_Nana[i + 1] = (temp[i] + 2 * temp[i + 1] + temp[i + 2]) / 4.0
    gCG_Nana[length - 1] = 0.5 * (temp[length-2] + temp[length-1])

for j in range(1):  # (order of smoothing)
    print "smooth g"
    temp = gCG_Nacl
    for i in range(length - 2):
        if i > 220:
            gCG_Nacl[i + 1] = (temp[i] + 2 * temp[i + 1] + temp[i + 2]) / 4.0
    gCG_Nacl[length - 1] = 0.5 * (temp[length-2] + temp[length-1])

for j in range(1):  # (order of smoothing)
    temp = gCG_Clcl
    for i in range(length - 2):
        gCG_Clcl[i + 1] = (temp[i] + 2 * temp[i + 1] + temp[i + 2]) / 4.0
    gCG_Clcl[length - 1] = 0.5 * (temp[length-2] + temp[length-1])

#
####################################################################
# Evaluate total error in g of r in the last run compared to AA

errorNana = 0
errorNacl = 0
errorClcl = 0

newF = np.zeros(length)
for i in range(length - 1):
    errorNana += weight[i] * (CGgofr_Nana[i][1] - AAgofr_Nana[i, 1]) ** 2
    errorNacl += weight[i] * (CGgofr_Nacl[i][1] - AAgofr_Nacl[i, 1]) ** 2
    errorClcl += weight[i] * (CGgofr_Clcl[i][1] - AAgofr_Nana[i, 1]) ** 2
errorNana /= length 
errorNacl /= length
errorClcl /= length
errorTotal = errorClcl + errorNacl + errorNana

with open('error.txt', 'w') as f:
    f.write('Nana square error:\n')
    f.write(str(errorNana)+'\t')
    f.write(str(errorNacl)+'\t')
    f.write('Total square error:\n')
    f.write(str(errorTotal)+'\t')

# get PMF from g of r ----------------------------------------------------------------------
AAPMF_Nana = np.zeros(length)
AAPMF_Clcl = np.zeros(length)
CGPMF_Nana = np.zeros(length)
CGPMF_Clcl = np.zeros(length)

AAPMF_Nana[0:length-33] = -np.log(gAA_Nana[33:length])
AAPMF_Nacl = -np.log(gAA_Nacl)
AAPMF_Clcl[0:length-75] = -np.log(gAA_Clcl[75:length])

CGPMF_Nana[0:length-33] = -np.log(gCG_Nana[33:length])  # this is going to give + inf
CGPMF_Nacl = -np.log(gCG_Nacl)
CGPMF_Clcl[0:length-75] = -np.log(gCG_Clcl[75:length])

###########
# Na-Na ###
###########

diff = (AAPMF_Nana - CGPMF_Nana) * weight
newPMF = oldPMF_Nana[:, 1] + alpha * diff

# fix the inf

for l in range(2):
    temp = newPMF
    for k in range(length-2, 0, -1):

        if temp[k-1] == np.inf or temp[k-1] == -np.inf or np.isnan(temp[k-1]):

            newPMF[k] = newPMF[k + 1] * expfactor
        else:
            newPMF[k] = (temp[k+1] + 2 * temp[k] + temp[k-1]) / 4.0
newPMF[1] = expfactor * newPMF[2]
newPMF[0] = expfactor * newPMF[1]

for i in range(length-50, length, 1):
    newPMF[i] = newPMF[i-1] * 0.8

# get Force

for i in range(length - 2):
    newF[i + 1] = (newPMF[i] - newPMF[i + 2]) * 250
newF[0] = (newPMF[0] - newPMF[1]) * 500
newF[length - 1] = 0

for j in range(1):
    F_temp = newF
    for i in range(length-2):
        if F_temp[i] < 80:
            newF[i+1] = (F_temp[i] + 2 * F_temp[i+1] + F_temp[i+2]) / 4.0
    newF[length-1] = 0
# export force
dist_col = dist1.reshape(-1, 1)
newPMF_col = newPMF.reshape(-1, 1)
newF_col = newF.reshape(-1, 1)

newNana = np.concatenate((dist_col, newPMF_col, newF_col), axis=1)

np.savetxt('midNana.dat', newNana, delimiter=' ')

##########
# Na-Cl ##
##########

print "Calculating new potential for Nacl......\n"
diff = (AAPMF_Nacl - CGPMF_Nacl) * weight
newPMF = oldPMF_Nacl[:, 1] + alpha * diff

# fix the inf
for l in range(1):
    temp = newPMF
    for k in range(length-2, 0, -1):

        if temp[k-1] == np.inf or temp[k-1] == -np.inf or np.isnan(temp[k-1]):

            newPMF[k] = newPMF[k + 1] * expfactor

        else:
            newPMF[k] = (temp[k+1] + 2 * temp[k] + temp[k-1]) / 4.0

newPMF[1] = expfactor * newPMF[2]
newPMF[0] = expfactor * newPMF[1]
for i in range(length-50, length, 1):
    newPMF[i] = newPMF[i-1] * 0.8

# get Force

for i in range(length - 2):
    newF[i + 1] = (newPMF[i] - newPMF[i + 2]) * 250
newF[0] = (newPMF[0] - newPMF[1]) * 500
newF[length - 1] = 0

for j in range(1):
    F_temp = newF
    for i in range(length-2):
        if F_temp[i] < 80:
            newF[i+1] = (F_temp[i] + 2 * F_temp[i+1] + F_temp[i+2]) / 4.0
    newF[length-1] = 0

# export Force
dist_col = dist2.reshape(-1, 1)
newPMF_col = newPMF.reshape(-1, 1)
newF_col = newF.reshape(-1, 1)
newNacl = np.concatenate((dist_col, newPMF_col, newF_col), axis=1)
np.savetxt('midNacl.dat', newNacl, delimiter=' ')


###########
# Cl-Cl ###
###########

diff = (AAPMF_Clcl - CGPMF_Clcl) * weight
newPMF = oldPMF_Clcl[:, 1] + alpha * diff

# fix the inf
for l in range(1):
    temp = newPMF
    for k in range(length-2, 0, -1):
        if temp[k-1] == np.inf or temp[k-1] == -np.inf or np.isnan(temp[k-1]):
            newPMF[k] = newPMF[k + 1] * expfactor

        else:
            newPMF[k] = (temp[k+1] + 2 * temp[k] + temp[k-1]) / 4.0
newPMF[1] = expfactor * newPMF[2]
newPMF[0] = expfactor * newPMF[1]

# get Force

for i in range(length - 2):
    newF[i + 1] = (newPMF[i] - newPMF[i + 2]) * 250
newF[0] = (newPMF[0] - newPMF[1]) * 500
newF[length - 1] = 0

for j in range(1):
    F_temp = newF
    for i in range(length-2):
        if F_temp[i] < 50:
            newF[i+1] = (F_temp[i] + 2 * F_temp[i+1] + F_temp[i+2]) / 4.0
    newF[length-1] = 0

dist_col = np.reshape(dist3, (-1, 1))
newPMF_col = newPMF.reshape(-1, 1)
newF_col = np.reshape(newF, (-1, 1))
newClcl = np.concatenate((dist_col, newPMF_col, newF_col), axis=1)
np.savetxt('midClcl.dat', newClcl, delimiter=' ')
print "newClcl saved !"
