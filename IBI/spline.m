data=importdata('midNana.dat');
x=data(:,1);
y=data(:,2);
f=fit(x,y,'smoothingspline','SmoothingParam',0.9999999);
spl=f.p;
[breaks,coefs,l,k,d]=unmkpp(spl);
Dspl = mkpp(breaks, repmat(k-1 : -1 : 1, d*l, 1).*coefs(:,1:k-1),d);
force = -ppval(Dspl, x);
test=horzcat(x,f(x),force);
dlmwrite('newNana.dat',test,' ')

data=importdata('midNacl.dat');
x=data(:,1);
y=data(:,2);
f=fit(x,y,'smoothingspline','SmoothingParam',0.99999999);
spl=f.p;
[breaks,coefs,l,k,d]=unmkpp(spl);
Dspl = mkpp(breaks, repmat(k-1 : -1 : 1, d*l, 1).*coefs(:,1:k-1),d);
force = -ppval(Dspl, x);
test=horzcat(x,f(x),force);
dlmwrite('newNacl.dat',test,' ')

data=importdata('midClcl.dat');
x=data(:,1);
y=data(:,2);
f=fit(x,y,'smoothingspline','SmoothingParam',0.9999999);
spl=f.p;
[breaks,coefs,l,k,d]=unmkpp(spl);
Dspl = mkpp(breaks, repmat(k-1 : -1 : 1, d*l, 1).*coefs(:,1:k-1),d);
force = -ppval(Dspl, x);
test=horzcat(x,f(x),force);
dlmwrite('newClcl.dat',test,' ')
