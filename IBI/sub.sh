#!/bin/bash
#MSUB -N "pmf-np"
#MSUB -l nodes=1:ppn=1:gpus=1
#MSUB -l feature=k80
#MSUB -A b1030
#MSUB -q buyin
#MSUB -l walltime=8:00:00
#MSUB -V


########################################################################################
# Load required modules
# unset CUDA_VISIBLE_DEVICES
# module load python
module load matlab/r2015a
module load mpi4py
module load cuda/cuda_7.5.18
module unload mpi
module load mpi/mpich-3.0.4-gcc-4.8.3

LD_LIBRARY_PATH=/software/anaconda2/lib:$LD_LIBRARY_PATH
LD_LIBRARY_PATH=/projects/b1030/boost/lib:$LD_LIBRARY_PATH 
###################################################################################

for order in 1 2 3 4 5 6 7 8 

do cd /home/ylc677/Ion_gofr/1m_sp
   mkdir ion_iter_${order}
   last=$(($order -1))
   cd ion_iter_${order}
   cp ../*py .
   cp ../*.tcl .
   cp ../AA* .
   cp ../spline.m .

   cp ../newNana_${last}.dat .
   cp ../newNacl_${last}.dat .
   cp ../newClcl_${last}.dat .
   mpirun -n 1 python ion-table.py --user="newNana_${last}.dat newNacl_${last}.dat newClcl_${last}.dat"
   vmd -hoomd final.xml traj.dcd -dispdev text -e calculate_gofr_Nana.tcl -args gofr_Nana.dat
   vmd -hoomd final.xml traj.dcd -dispdev text -e calculate_gofr_Nacl.tcl -args gofr_Nacl.dat
   vmd -hoomd final.xml traj.dcd -dispdev text -e calculate_gofr_Clcl.tcl -args gofr_Clcl.dat
   python dataprocess.py newNana_${last}.dat newNacl_${last}.dat newClcl_${last}.dat
   
   # dataprocess.py generates newNana files, move to upper level to be accessed by next iteration
   matlab -nosplash -nodisplay -nodesktop < spline.m
   mv newNana.dat ../newNana_${order}.dat
   mv newNacl.dat ../newNacl_${order}.dat
   mv newClcl.dat ../newClcl_${order}.dat
   # copy the new potential files to the upper directory
   file="error.txt"
   error=$(cat "$file")
   cd ..
   echo $error>>Error.txt

done
