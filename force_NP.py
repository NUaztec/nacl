import sys
import os
sys.path.append('/projects/b1030/hoomd/hoomd-2.1.5/')  # the path to HOOMD in your cluster.
sys.path.append('/projects/b1030/hoomd/hoomd-2.1.5/hoomd/')
from hoomd import *
from hoomd.md import *
from hoomd import deprecated as hdepr
import numpy as np
import random


def zerof(r, rmin, rmax, epsilon, sigma):
    return (0, 0)


def pot_shift(rc, epsilon, sigma, delta):
    sig_r3 = (sigma / (rc - delta))*(sigma / (rc - delta)) * (sigma / (rc - delta))
    sig_r_delta6 = sig_r3 * sig_r3
    vrc = 4 * epsilon * sig_r_delta6 * (sig_r_delta6 - 1)
    return vrc

def shift_lj(r, rmin, rmax, epsilon, sigma, delta, shift):
    ri2 = (sigma/(r-delta))*(sigma/(r-delta))
    sig_r_delta6 = ri2 * ri2 * ri2
    potential = 4 * epsilon * (sig_r_delta6 * sig_r_delta6 - sig_r_delta6) - shift
    f = 4 * epsilon / r * (12 * sig_r_delta6 * (sig_r_delta6 - 0.5)) 
    return (potential, f)


def cback_fx0(tstep):
    p = system.particles[0]
    forcex = p.net_force[0]
    return forcex


def cback_fx1(tstep):
    p = system.particles[1]
    forcex = p.net_force[0]
    return forcex


def cback_dist(tstep):
    r = system.particles[0].position[0] - system.particles[1].position[0]
    return r

def cbadk_j(tstep):
    return j


context.initialize()
arg = option.get_user()
charge1 = int(arg[0]) 
job_id = int(arg[1])
print 'charge1 = ' + str(charge1) + '\n'
# constants
num_avg = 1000
intv = 150
force_sample_num = intv*num_avg 

diameter1 = 8.0
blx = 40
bly = 24
init_pos = 4.5
final_pos = 7.5
molarity = 1.0
stepsize = 0.05
radi_Na = 0.15
radi_Cl = 0.21

ion_charge = 7.49 / np.sqrt(73.53)
chargeSphere = charge1 * ion_charge

positions = np.arange(init_pos, final_pos + 0.00001, stepsize)

N_A = 6.022 / 10.0  # Avogadro's Constant reduced
vol = blx * bly**2 - (np.pi) * (diameter1 ** 2 / 2.0)
num_ion = int(vol * molarity * N_A)  # positive or negative, only one copy!
num_anion = int(2 * charge1)
num_slices = len(positions)

nve_time = 2e3  # simulation parameters
nvt_time = 1e6
timestep = 0.001
num_particles = 2 * num_ion + num_anion + 2
print 'position from\t' + str(init_pos) + 'to\t' + str(final_pos)
print positions
print("number of particles: " + str(num_particles))

snapshot = data.make_snapshot(N=num_particles, particle_types=['A', 'B', 'C', 'D'], box=data.boxdim(Lx=blx, Ly=bly, Lz=bly))
snapshot.box = data.boxdim(Lx=blx, Ly=bly, Lz=bly, xy=0.0, xz=0.0, yz=0.0)  # elongated in x direction
system = init.read_snapshot(snapshot)

typeid_array = np.array([0, 1])
init_coord = np.array([init_pos, 0, 0])
for i in range(2):  # initialize two nps
    snapshot.particles.position[i] = ((-1)**i * init_coord)
    snapshot.particles.velocity[i] = (0, 0, 0)
    snapshot.particles.charge[i] = chargeSphere
    snapshot.particles.typeid[i] = typeid_array[i]
    snapshot.particles.diameter[i] = diameter1

idx = 2  # start from the 2nd particle, which is the first ion
delta_r = 0.6
list_ions = []
overlapCount = 0
ionCount = 0
clCount = 0
anionCount = 0
d2 = radi_Cl**2
forbiddenR1 = np.square(0.5 * diameter1 + radi_Cl + delta_r)
print 'Add ions'
while True:
    overlap = False
    x = blx * 0.5 * (2.0 * random.random() - 1.0)
    y = bly * 0.5 * (2.0 * random.random() - 1.0)
    z = bly * 0.5 * (2.0 * random.random() - 1.0)
    dist_0 = np.square(x - init_coord[0]) + np.square(y - init_coord[1]) + np.square(z - init_coord[2])
    dist_1 = np.square(x + init_coord[0]) + np.square(y + init_coord[1]) + np.square(z + init_coord[2])

    if dist_0 < forbiddenR1 or dist_1 < forbiddenR1:
        overlap = True
        overlapCount += 1
    elif len(list_ions) > 0:  # check if overlap with other ions

        for pos_ions in list_ions:
            x0 = pos_ions[0] - x
            y0 = pos_ions[1] - y
            z0 = pos_ions[2] - z

            if x0 * x0 + y0 * y0 + z0 * z0 < d2:
                overlap = True
                break
            else:
                overlap = False
                # print 'ion' + str(idx)

    # add particle
    if not overlap:
        list_ions.append([x, y, z])
        snapshot.particles.position[idx] = (x, y, z)

        # assign charge  # if ion size were different, this has to be moved before comparing for judgement
        if idx < num_ion + 1.1:
            snapshot.particles.charge[idx] = ion_charge
            snapshot.particles.typeid[idx] = 2  # type C
            snapshot.particles.diameter[idx] = 2 * radi_Cl
            snapshot.particles.velocity[idx] = np.random.normal(size=3)
            ionCount += 1
        elif idx < 2 * num_ion + 1.1:
            snapshot.particles.charge[idx] = -ion_charge  # anions and additional ions added
            snapshot.particles.typeid[idx] = 3  # type D
            snapshot.particles.diameter[idx] = 2 * radi_Cl
            snapshot.particles.velocity[idx] = np.random.normal(size=3)
            snapshot.particles.mass[idx] = 35 / 23.5
            clCount += 1
        else:
            if charge1 > 0:
                snapshot.particles.charge[idx] = -ion_charge  # anions and additional ions added
                snapshot.particles.typeid[idx] = 3  # type D
                snapshot.particles.diameter[idx] = 2 * radi_Cl
                snapshot.particles.velocity[idx] = np.random.normal(scale=0.8, size=3)
                snapshot.particles.mass[idx] = 35 / 23.5  # mass is different
                anionCount += 1
            elif charge1 < 0:
                snapshot.particles.charge[idx] = ion_charge
                snapshot.particles.typeid[idx] = 2  # type C
                snapshot.particles.diameter[idx] = 2 * radi_Cl
                snapshot.particles.velocity[idx] = np.random.normal(size=3)
                anionCount += 1

        idx += 1
    if idx == num_particles:
        print 'ions added, overlap = '+str(overlapCount)
        break

system.restore_snapshot(snapshot)
print 'Na' + str(ionCount)
print 'Cl' + str(clCount)
print 'Anion' + str(anionCount)

xml = hdepr.dump.xml(group=group.all(), filename="init{}.xml".format(job_id), vis=True)

sig = 1.0
epsi = 0.001

delta_Na = radi_Na + diameter1 / 2.0
delta_Cl = radi_Cl + diameter1 / 2.0

rcut2 = 2**(1/6.0)
vrcNa0 = pot_shift(delta_Na+rcut2, epsi, 1.0, delta_Na)
vrcCl0 = pot_shift(delta_Cl+rcut2, epsi, 1.0, delta_Cl)

nl = nlist.cell()
table = pair.table(width=650, nlist=nl)
table.pair_coeff.set('A', 'A', func=zerof,rmin=1,rmax=1.2,coeff=dict(epsilon=0,sigma=1.0))
table.pair_coeff.set('A', 'B', func=zerof,rmin=1,rmax=1.2,coeff=dict(epsilon=0,sigma=1.0))
table.pair_coeff.set('B', 'B', func=zerof,rmin=1,rmax=1.2,coeff=dict(epsilon=0,sigma=1.0))
table.pair_coeff.set('A', 'C', func=shift_lj, rmin=delta_Na + 0.001, rmax=(delta_Na + rcut2), coeff=dict(epsilon=epsi, sigma=1.0, delta=delta_Na, shift=vrcNa0))
table.pair_coeff.set('A', 'D', func=shift_lj, rmin=delta_Cl + 0.001, rmax=(delta_Cl + rcut2), coeff=dict(epsilon=epsi, sigma=1.0, delta=delta_Cl, shift=vrcCl0))
table.pair_coeff.set('B', 'C', func=shift_lj, rmin=delta_Na + 0.001, rmax=(delta_Na + rcut2), coeff=dict(epsilon=epsi, sigma=1.0, delta=delta_Na, shift=vrcNa0))
table.pair_coeff.set('B', 'D', func=shift_lj, rmin=delta_Cl + 0.001, rmax=(delta_Cl + rcut2), coeff=dict(epsilon=epsi, sigma=1.0, delta=delta_Cl, shift=vrcCl0))
table.set_from_file('C', 'C', filename='newNaNa-1m.dat')
table.set_from_file('C', 'D', filename='newNaCl-1m.dat')
table.set_from_file('D', 'D', filename='newClCl-1m.dat')
all = group.all()
typeA = group.type(name='a-particles', type='A')
typeB = group.type(name='b-particles', type='B')
typeC = group.type(name='c-particles', type='C')
typeD = group.type(name='d-particles', type='D')

groupcenter = group.rigid_centers()
dump_gsd = dump.gsd(group = groupcenter)

typeIons = group.union(name='ions', a=typeC, b=typeD)
typeNPs = group.union(name='nps', a=typeA, b=typeB)

charged = group.charged()  # pppm section
nl = nlist.cell()
pppm = charge.pppm(group=charged, nlist=nl)
pppm.set_params(Nx=128, Ny=128, Nz=128, order=4, rcut=2.1)

integrate.mode_standard(dt=timestep)
Qlog = ['temperature', 'potential_energy', 'pair_table_energy']
logger = analyze.log(quantities=Qlog, period=2e3, filename="test{}.log".format(job_id), overwrite=True)


integrator = integrate.nve(typeIons, limit=0.01)
zero = update.zero_momentum(period=200)
# dcd = dump.dcd(filename="traj{}.dcd".format(job_id), period=5e4, overwrite=True)
run(nve_time)
integrator.disable()
zero.disable()
integrate.mode_standard(dt=timestep)
integrator = integrate.langevin(group=typeIons, kT=1.0, seed=6, dscale=0.2)
# integrator.set_gamma('C', gamma=0.05)
# integrator.set_gamma('D', gamma=0.05) 
run(nvt_time)
logger.disable()
# dcd.disable()

Qlog.extend(['dist_idx', 'dist', 'av_fx0', 'av_fx1'])
logger = analyze.log(quantities=Qlog, period=intv, filename="run{}.log".format(job_id), overwrite=True)
logger.register_callback('av_fx0', cback_fx0)
logger.register_callback('av_fx1', cback_fx1)
logger.register_callback('dist', cback_dist)
logger.register_callback('dist_idx', cbadk_j)
moves = 50
dr = stepsize / moves * np.array([1, 0, 0])

for j in range(num_slices):
    if j > 0.5:
        # move nano particle
        logger.disable()
        for k in range(moves):
            pos0 = np.array(system.particles[0].position)
            pos1 = np.array(system.particles[1].position)
            (x0, y0, z0) = pos0 + dr
            (x1, y1, z1) = pos1 - dr
            system.particles[0].position = (x0, y0, z0)
            system.particles[1].position = (x1, y1, z1)
            run(200)
        # move
        #  finished
        run(2e4)  # to let it equilibriate
        logger.enable()

   # if j>55:
   #     dcd = dump.dcd(filename="traj{}-{}.dcd".format(job_id,j), period=1e3, overwrite=True)
    run(force_sample_num)

integrator.disable()
xml = hdepr.dump.xml(group=group.all(), filename="final{}.xml".format(job_id), vis=True)

# data process
data = np.genfromtxt('run{}.log'.format(job_id), delimiter='\t')
size = len(data)
print size
points = size/num_avg

j = 1
for i in range(points):
    f1 = 0
    f2 = 0
    if j < size-2:
        dist = data[j, 5]
        while data[j, 4] == i:
            f1 += data[j, 6]
            f2 += data[j, 7]
            j += 1
            if j == size-1:
                break
    f1 /= num_avg
    f2 /= num_avg
    with open('npForce{}-{}.dat'.format(charge1,job_id), 'a') as f:
        f.write(str(dist) + '\t' + str(f1) + '\t' + str(f2) + '\n')

