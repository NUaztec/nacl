#! /usr/bin/env python

import argparse
from subprocess import *
import sys
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy import special
from scipy.interpolate import UnivariateSpline
from scipy.optimize import curve_fit

# update history
'''
ver 2.0: Improved custom file reading, corrected integration
ver 3.0: Switch to first spline fit then integrate.


'''
def shift_lj(r, rmin, rmax, epsilon, sigma, delta, shift):
    sig_r_delta6 = (sigma / (r - delta)) ** 6
    potential = 4 * epsilon * (sig_r_delta6 ** 2 - sig_r_delta6) - shift
    f = 4 * epsilon / r * (12 * sig_r_delta6 ** 2 - 6 * sig_r_delta6)  # only true for sigma = 1
    return (potential, f)

###########################################################
# constants
diameter = 8.0
ion_radius = 0.2
epsi = 0.001
sig = 1.0
cutoff = 2**(1/6.0)*sig
ion_charge = 7.49 / 73.53 ** 0.5

# Calculate the repulsive core ##############################
shift = shift_lj(cutoff, 2.0, 3.0, epsi, sig, diameter, 0)
# split into dist1 dist2 to get repulsive LJ
fine_step = 0.01
dist1 = np.arange(diameter + 0.01, diameter + cutoff, fine_step)
dist2 = np.arange(diameter + cutoff, diameter + 6, fine_step)
distance = np.append(dist1, dist2)
length = len(dist1) + len(dist2)
potn = np.zeros(length)
i = 0
for rr in dist1:
    softSphere = shift_lj(rr, 0, 1.0, epsi, sig, diameter, shift)
    potn[i] = softSphere[1]
    i += 1

# Read files from MD Simulation ################################
data1 = np.genfromtxt('npForce1000-15.dat', delimiter='\t')
l_data = len(data1)
dist = data1[:, 0]
outputName='potential_full_q1000.dat'
filenames = {'npForce1000-15.dat', 'npForce1000-18.dat','npForce1000-20.dat','npForce1000-16.dat','npForce1000-17.dat'}  # strings of names to read in
num_files = len(filenames)
data = np.zeros((num_files, l_data, 2))
force = np.zeros(l_data)
sigForce = np.zeros(l_data)
i = 0
for name in filenames:
    temp = np.genfromtxt(name, delimiter='\t')
    data[i, :, 0] = temp[:, 1]
    data[i, :, 1] = temp[:, 2]
    force += (temp[:, 1] - temp[:, 2])
    i += 1

force = force / 2.0 / num_files
print force

for i in range(num_files):
    sigForce += (np.square(data[i, :, 0] - force) + np.square(-data[i, :, 1]-force))
    
sigForce /= (2*num_files)
sigForce = np.sqrt(sigForce)

# Spline fitting and plotting force ##############################
spl_force = UnivariateSpline(dist, force)
spl_force.set_smoothing_factor(500)
fit_force = spl_force(distance)
fig1 = plt.figure()
plt.errorbar(dist, force, sigForce, fmt='ro')
plt.plot(distance[5:-1], spl_force(distance[5:-1]), linewidth=1.5)
fig1.suptitle('Mean force between NPs')
plt.xlabel('Center-Center Distance (nm)')
plt.ylabel('Mean Force (kT/nm)')
plt.grid(True)

distance_col = np.reshape(distance, (-1, 1))
dist_col = np.reshape(dist, (-1, 1))
force_col = np.reshape(force, (-1, 1))
sigForce_col = np.reshape(sigForce, (-1, 1))
force_out = np.concatenate((dist_col, force_col, sigForce_col), axis=1)
np.savetxt('Force.dat', force_out, delimiter=' ')
plt.ylim(-10, 30)
# plt.show()

# discrete numerical integration ##################################
potential = np.zeros(length)
potential[0] = 0

for i in range(length-1):
    rr = fine_step*i + diameter
    force_1 = spl_force(rr)
    force_2 = spl_force(rr+0.01)
    potential[i+1] = potential[i] - 0.5 * fine_step * (force_1 + force_2)

shift = potential[length-1]  # comment for calculating from far to close
potential -= shift
distance_col = np.reshape(distance, (-1, 1))
potential_col = np.reshape(potential, (-1, 1))
potentials = np.concatenate((distance_col, potential_col), axis=1)
V_full = potential[49:-1] + potn[49:-1]
V_col = np.reshape(V_full, (-1, 1))
V_Full_out = np.concatenate((distance_col[49:-1], V_col), axis=1)

np.savetxt(outputName, V_Full_out)
# plot potential and spline fit of potential
fig2 = plt.figure()
plt.plot(distance[49:-1], V_full, linewidth=2)
plt.ylabel('potential (kT)')
plt.xlabel('distance/nm')
plt.ylim(-4, 10)
plt.grid(True)
plt.show()
'''

F = spl_force(distance)

distance_col = np.reshape(distance, (-1, 1))
V_col = np.reshape(V_full, (-1, 1))
F_col = np.reshape(F, (-1, 1))
spherePotential = np.concatenate((distance_col, V_col, F_col), axis=1)
np.savetxt('spherePot.dat', spherePotential, delimiter=' ')

potential_col = np.reshape(potential, (-1, 1))
potentials = np.concatenate((distance_col, potential_col), axis=1)

np.savetxt('potentials.dat', potentials)
plt.plot(distance, V_full)
plt.ylabel('potential (kT)')
plt.ylim(-10, 5)
plt.xlabel('distance/nm')
plt.show()

'''
